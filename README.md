# Sublime settings #

These are my Sublime Text settings.
It contains the whole User folder, so:

* Snippets
* Key bindings
* Preferences
* Color schemes